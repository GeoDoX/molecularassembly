package com.geodox.molecularassembly.creative.tab;

import com.geodox.molecularassembly.MolecularAssembly;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;

/**
 * Created by GeoDoX on 2016-11-21.
 */
public class MACreativeTab
{
    // Create a new Creative Tab for Molecular Assembly
    public static final CreativeTabs TAB = new CreativeTabs(MolecularAssembly.Constants.modID)
    {
        @Override
        public Item getTabIconItem()
        {
            //return Item.getItemFromBlock(MolecularAssembly.ModBlocks.assemblyChamberParticleManipulator);
            return Items.NETHER_STAR;
        }
    };
}
