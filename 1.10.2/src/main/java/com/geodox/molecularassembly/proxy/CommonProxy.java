package com.geodox.molecularassembly.proxy;

import net.minecraft.item.Item;

/**
 * Created by GeoDoX on 2016-11-21.
 */
public class CommonProxy
{
    // NOTE: Happens Client Side ONLY, so do nothing here
    public void registerItemRenderer(Item item, int meta, String itemName) {}

    public void registerModelDomain() {}
}
