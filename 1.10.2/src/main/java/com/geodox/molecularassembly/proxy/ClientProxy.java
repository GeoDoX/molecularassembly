package com.geodox.molecularassembly.proxy;

import com.geodox.molecularassembly.MolecularAssembly;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.client.model.obj.OBJLoader;

/**
 * Created by GeoDoX on 2016-11-21.
 */
public class ClientProxy extends CommonProxy
{
    @Override
    public void registerItemRenderer(Item item, int meta, String itemName)
    {
        ModelLoader.setCustomModelResourceLocation(item, meta, new ModelResourceLocation(MolecularAssembly.Constants.modID + ":" + itemName, "inventory"));
    }

    @Override
    public void registerModelDomain()
    {
        OBJLoader.INSTANCE.addDomain(MolecularAssembly.Constants.modID);
    }
}
