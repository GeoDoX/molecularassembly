package com.geodox.molecularassembly;

import com.geodox.molecularassembly.block.*;
import com.geodox.molecularassembly.item.ItemMolecularAnalyzer;
import com.geodox.molecularassembly.item.ItemStabilizedMagneticDust;
import com.geodox.molecularassembly.item.MAItem;
import com.geodox.molecularassembly.proxy.CommonProxy;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.fml.common.FMLLog;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.IntBuffer;

/**
 * MolecularAssembly.java
 *
 * Created by GeoDoX on 2016-11-21.
 */
@Mod(modid = MolecularAssembly.Constants.modID, name = MolecularAssembly.Constants.modName, version = MolecularAssembly.Constants.modVersion)
public class MolecularAssembly
{
    @Mod.Instance(Constants.modID)
    public static MolecularAssembly instance;

    @SidedProxy(serverSide = Constants.serverProxy, clientSide = Constants.clientProxy)
    public static CommonProxy proxy;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
        proxy.registerModelDomain();

        ModItems.init();
        ModBlocks.init();
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event)
    {
        ModRecipes.init();
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event)
    {

    }

    public static class ModItems
    {
        public static MAItem molecularAnalyzer;
        public static MAItem stabilizedMagneticDust;

        public static void init()
        {
            molecularAnalyzer = register(new ItemMolecularAnalyzer());
            stabilizedMagneticDust = register(new ItemStabilizedMagneticDust());
        }

        private static <T extends Item> T register(T item)
        {
            GameRegistry.register(item);

            if (item instanceof MAItem)
                ((MAItem)item).registerItemModel();

            return item;
        }
    }

    public static class ModBlocks
    {
        public static MABlock assemblyChamberParticleManipulator;
        public static MABlock assemblyChamberParticleSeparator;
        public static MABlock assemblyChamberWall;
        public static MABlock assemblyChamberWindow;
        public static MABlock stabilizedMagneticOre;

        public static void init()
        {
            assemblyChamberParticleManipulator = register(new BlockAssemblyChamberParticleManipulator());
            assemblyChamberParticleSeparator = register(new BlockAssemblyChamberParticleSeparator());
            assemblyChamberWall = register(new BlockAssemblyChamberWall());
            assemblyChamberWindow = register(new BlockAssemblyChamberWindow());
            //stabilizedMagneticOre = register(new BlockStabilizedMagneticOre());
        }

        private static <T extends Block> T register(T block, ItemBlock itemBlock)
        {
            GameRegistry.register(block);
            GameRegistry.register(itemBlock);

            if (block instanceof MABlock)
                ((MABlock)block).registerItemModel(itemBlock);

            return block;
        }

        private static <T extends Block> T register(T block)
        {
            ItemBlock itemBlock = new ItemBlock(block);
            itemBlock.setRegistryName(block.getRegistryName());
            return register(block, itemBlock);
        }
    }

    public static class ModRecipes
    {
        public static void init()
        {

        }
    }

    public static class Constants
    {
        // Mod Information
        public static final String modID = "molecularassembly";
        public static final String modName = "Molecular Assembly";
        public static final String modVersion = "1.0.0";

        // Proxy Information
        protected static final String serverProxy = "com.geodox.molecularassembly.proxy.CommonProxy";
        protected static final String clientProxy = "com.geodox.molecularassembly.proxy.ClientProxy";

        public static class Blocks
        {
            public static final String unlocalizedBlockAssemblyChamberWall = "assemblyChamberWall";
            public static final String unlocalizedBlockAssemblyChamberWindow = "assemblyChamberWindow";
            public static final String unlocalizedBlockAssemblyChamberParticleSeparator = "assemblyChamberParticleSeparator";
            public static final String unlocalizedBlockAssemblyChamberParticleManipulator = "assemblyChamberParticleManipulator";
            public static final String unlocalizedBlockStabilizedMagneticOre = "stabilizedMagneticOre";
        }

        public static class Items
        {
            public static final String unlocalizedItemMolecularAnalyzer = "molecularAnalyzer";
            public static final String unlocalizedItemStabilizedMagneticDust = "stabilizedMagneticDust";
        }
    }
}
