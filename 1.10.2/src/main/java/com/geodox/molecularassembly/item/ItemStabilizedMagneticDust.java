package com.geodox.molecularassembly.item;

import com.geodox.molecularassembly.MolecularAssembly;

/**
 * Created by GeoDoX on 2016-11-21.
 */
public class ItemStabilizedMagneticDust extends MAItem
{
    public ItemStabilizedMagneticDust()
    {
        super(MolecularAssembly.Constants.Items.unlocalizedItemStabilizedMagneticDust);
    }
}
