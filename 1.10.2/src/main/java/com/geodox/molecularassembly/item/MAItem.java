package com.geodox.molecularassembly.item;

import com.geodox.molecularassembly.MolecularAssembly;
import com.geodox.molecularassembly.creative.tab.MACreativeTab;
import net.minecraft.item.Item;

/**
 * Created by GeoDoX on 2016-11-21.
 */
public class MAItem extends Item
{
    protected String itemName;

    public MAItem(String unlocalizedName)
    {
        super();

        this.itemName = unlocalizedName;

        this.setUnlocalizedName(MolecularAssembly.Constants.modID + "." + itemName);
        this.setRegistryName(itemName);
        this.setCreativeTab(MACreativeTab.TAB);
    }

    public void registerItemModel()
    {
        MolecularAssembly.proxy.registerItemRenderer(this, 0, itemName);
    }

    public String getItemName()
    {
        return itemName;
    }
}
