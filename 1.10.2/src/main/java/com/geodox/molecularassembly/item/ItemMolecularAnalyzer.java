package com.geodox.molecularassembly.item;

import com.geodox.molecularassembly.MolecularAssembly;

/**
 * Created by GeoDoX on 2016-11-21.
 */
public class ItemMolecularAnalyzer extends MAItem
{
    public ItemMolecularAnalyzer()
    {
        super(MolecularAssembly.Constants.Items.unlocalizedItemMolecularAnalyzer);
    }
}
