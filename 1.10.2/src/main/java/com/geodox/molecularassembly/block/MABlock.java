package com.geodox.molecularassembly.block;

import com.geodox.molecularassembly.MolecularAssembly;
import com.geodox.molecularassembly.creative.tab.MACreativeTab;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.ItemBlock;

/**
 * Created by GeoDoX on 2016-11-21.
 */
public class MABlock extends Block
{
    protected String blockName;

    public MABlock(String unlocalizedName)
    {
        this(unlocalizedName, Material.ROCK);
    }

    public MABlock(String unlocalizedName, Material material)
    {
        super(material);

        this.blockName = unlocalizedName;

        this.setUnlocalizedName(MolecularAssembly.Constants.modID + "." + blockName);
        this.setRegistryName(blockName);
        this.setCreativeTab(MACreativeTab.TAB);
    }

    public void registerItemModel(ItemBlock itemBlock)
    {
        MolecularAssembly.proxy.registerItemRenderer(itemBlock, 0, blockName);
    }

    public String getBlockName()
    {
        return blockName;
    }
}