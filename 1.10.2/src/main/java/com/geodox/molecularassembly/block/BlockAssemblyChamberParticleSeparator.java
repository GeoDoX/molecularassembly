package com.geodox.molecularassembly.block;

import com.geodox.molecularassembly.MolecularAssembly;
import net.minecraft.block.material.Material;

/**
 * Created by GeoDoX on 2016-11-21.
 */
public class BlockAssemblyChamberParticleSeparator extends MABlock
{
    public BlockAssemblyChamberParticleSeparator()
    {
        super(MolecularAssembly.Constants.Blocks.unlocalizedBlockAssemblyChamberParticleSeparator, Material.IRON);
    }
}
