package com.geodox.molecularassembly.block;

import com.geodox.molecularassembly.MolecularAssembly;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Created by GeoDoX on 2016-11-21.
 */
public class BlockAssemblyChamberWindow extends MABlock
{
    public BlockAssemblyChamberWindow()
    {
        super(MolecularAssembly.Constants.Blocks.unlocalizedBlockAssemblyChamberWindow, Material.GLASS);
        this.setLightOpacity(0);
    }

    /* Not actually deprecated */
    @Override
    public boolean isOpaqueCube(IBlockState state)
    {
        return false;
    }

    @SideOnly(Side.CLIENT)
    public BlockRenderLayer getBlockLayer()
    {
        return BlockRenderLayer.CUTOUT;
    }
}
