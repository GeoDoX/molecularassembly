package com.geodox.molecularassembly.block;

import com.geodox.molecularassembly.MolecularAssembly;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;
import net.minecraft.util.math.MathHelper;

import java.util.Random;

/**
 * Created by GeoDoX on 2016-11-21.
 */
public class BlockStabilizedMagneticOre extends MABlock
{
    public BlockStabilizedMagneticOre()
    {
        super(MolecularAssembly.Constants.Blocks.unlocalizedBlockStabilizedMagneticOre, Material.IRON);
    }

    @Override
    public Item getItemDropped(IBlockState blockState, Random random, int fortune)
    {
        return MolecularAssembly.ModItems.stabilizedMagneticDust;
    }

    @Override
    public int quantityDropped(Random rand)
    {
        return MathHelper.getInt(rand, 1, 3);
    }
}
